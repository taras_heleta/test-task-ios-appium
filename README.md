# README #

### Test task, tests for mobile iOS application ###

### How do I get set up? ###
* Install missing package manager for macOS "Homebrew" using following command line instruction: 
`mkdir homebrew && curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C homebrew`
* Install Node.js: 
  `brew install node`
* Install Appium:
`npm install -g appium`
* Install libimobiledevice library:
`brew install libimobiledevice --HEAD`
* Install ideviceinstaller library:
`brew install ideviceinstaller`
* Install Carthage - dependency manager for Cocoa:
`brew install carthage`
* Install ios-deploy tool:
`npm install -g ios-deploy`
* Install xcpretty gem:
`gem install xcpretty`
* Install authorize-ios utility:
`npm install -g authorize-ios`
`sudo authorize-ios`
* Install WebDriverAgent:
`npm install wd`
* Run tests:
`bundle exec cucumber features --format html --out reports/report.html`