Feature: Simple Photos application

  Background: Application is running and alert was accepted

  Scenario: Verify default albums
    Then The next albums should be displayed:
      | album_name       |
      | All Photos       |
      | Favorites        |
      | Recently Deleted |
      | Panoramas        |
      | Camera Roll      |
      | Slo-mo           |
      | Screenshots      |
      | Bursts           |
      | Videos           |
      | Selfies          |
      | Hidden           |
      | Time-lapse       |
      | Recently Added   |
    And I scroll "down"
    Then The next albums should be displayed:
      | album_name    |
      | Animated      |
      | Long Exposure |
      | Portrait      |
      | Live Photos   |

  Scenario: Add new album
    When I click on "Add" button
    Then I type "awesome photos" text
    When I click on "Create" button
    And I scroll "down"
    Then Album "awesome photos" should be displayed

  Scenario: Add new photo
    When I open "All Photos" album
    And I remember amount of photos
    And I click on "Add" button
    Then amount of images should increased

  Scenario: Ensure that added photo moved to the Recently Added
    When I open "All Photos" album
    And I click on "Add" button
    And I click on "Photos" button
    When I open "Recently Added" album
    Then album should not be empty

  Scenario: Remove existing photo
    When I open "All Photos" album
    And I remember amount of photos
    And I click on first image
    When I click on "Delete" button
    And I accept alert
    Then amount of images should be decreased

  Scenario: Ensure that deleted photo moved to the Recently Deleted album
    When I open "All Photos" album
    And I click on first image
    When I click on "Delete" button
    And I accept alert
    And I click on "Photos" button
    When I open "Recently Deleted" album
    Then album should not be empty

  Scenario: Add photo to favorites
    When I open "All Photos" album
    And I click on first image
    And I click on "♡" button
    And I click on "All Photos" button
    And I click on "Photos" button
    And I open "Favorites" album
    Then album should not be empty

  Scenario: Verify Edit menu elements
    When I open "All Photos" album
    And I click on first image
    When I click on "Edit" button
    Then The next buttons should be displayed:
      | button_name |
      | Sepia Tone  |
      | Chrome      |
      | Revert      |
      | Cancel      |
