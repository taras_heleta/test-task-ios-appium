[caps]
platformName = "ios"
deviceName = "iPhone 6"
platformVersion = "10.2"
app = "../../../apps/TestApp/build/release-iphonesimulator/TestApp-iphonesimulator.app"
automationName = 'XCUITest'

[appium_lib]
sauce_username = false
sauce_access_key = false




caps: {# 'browserName' => 'iOS',
             'platformName' => 'ios',
             'deviceName' => 'iPhone 6',
             'version' => '6.0',
             'automationName' => 'XCUITest',
             'autoAcceptAlerts' => 'true',
             'app' => absolute_app_path,
             'newCommandTimeout' => '20000'},
      'appium_lib' => {
          sauce_username: nil,
          sauce_access_key: nil,
          wait: 60