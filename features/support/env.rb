require 'rspec'
require 'pry'
require 'rspec/expectations'
require 'appium_lib'
require 'cucumber/ast'
require_relative '../../lib/common_vars.rb'

APP_PATH = '../../app/SamplePhotosApp.app'

class AppiumWorld
end

def desired_caps
  {
      'caps' => {
          'platformName' => CommonVars::PLATFORM_NAME,
          'deviceName' => CommonVars::DEVICE_NAME,
          'platformVersion' => CommonVars::PLATFORM_VERSION,
          'automationName' => CommonVars::AUTOMATION_NAME,
          'autoAcceptAlerts' => CommonVars::AUTO_ACCEPT_ALERTS,
          'app' => absolute_app_path,
          'newCommandTimeout' => '20000',
          'fullReset' => CommonVars::FULL_RESET
      },
      'appium_lib' => {
          'sauce_username' => false,
          'sauce_access_key' => false,
      }
  }
end

def absolute_app_path
  File.join(File.dirname(__FILE__), APP_PATH)
end

Appium::Driver.new(desired_caps)
Appium.promote_appium_methods AppiumWorld

World do
  AppiumWorld.new
end

Before do
  $driver.start_driver
  alert_accept
end

After do |scenario|
  save_screenshot(scenario.name) if scenario.failed?
  $driver.driver_quit
end
