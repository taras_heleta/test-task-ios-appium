Then(/^The next albums should be displayed:$/) do |table|
  all_texts_str = texts.map {|el| el.text}.join(' ')
  expected_albums = table.hashes.map {|el| el.values.first}

  expected_albums.each do |album|
    expect(all_texts_str).to include(album)
  end
end

When(/^I click on "([^"]*)" button$/) do |btn_name|
  wait {button(btn_name).click}
end

And(/^I type "([^"]*)" text$/) do |text|
  first_textfield.send_keys(text)
end

Then(/^Album "([^"]*)" should be displayed$/) do |text|
  expect(find_element(name: text).displayed?).to be(true)
end

And(/^I scroll "([^"]*)"$/) do |direction|
  scroll direction: direction
end

When(/^I open "([^"]*)" album$/) do |album_bane|
  wait {find(album_bane).click}
end

And(/^I remember amount of photos$/) do
  @amount_of_images = find_elements(:class, 'XCUIElementTypeImage').count
end

Then(/^amount of images should increased$/) do
  expect(@amount_of_images + 1).to be(find_elements(:class, 'XCUIElementTypeImage').count)
end

And(/^I click on first image$/) do
  img = find_elements(:class, 'XCUIElementTypeImage').first
  wait {tap(x: 0, y: 0, element: img)}
end

And(/^I accept alert$/) do
  wait {alert_accept}
end

Then(/^amount of images should be decreased$/) do
  wait_true {expect(@amount_of_images - 1).to be(find_elements(:class, 'XCUIElementTypeImage').count)}
end

Then(/^album should not be empty$/) do
  expect(find_elements(:class, 'XCUIElementTypeImage').count).not_to equal(0)
end

Then(/^The next buttons should be displayed:$/) do |table|
  buttons_str = find_elements(:class, "XCUIElementTypeButton").map{|e| e.text}.join(' ')
  expected_buttons = table.hashes.map {|el| el.values.first}

  expected_buttons.each do |btn|
    expect(buttons_str).to include(btn)
  end
end